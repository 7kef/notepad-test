﻿
using NUnit.Framework;

using TestStack.White.UIItems.WindowItems;
using TestStack.White.Factory;
using System.Diagnostics;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems;
using TestStack.White.UIItems.WindowStripControls;
using TestStack.White;
using TestStack.White.UIItems.MenuItems;
using System.Windows.Automation;
using TestStack.White.UIItems.TreeItems;
using System.Threading;
using TestStack.White.UIItems.ListBoxItems;
using System.Collections.Generic;
using TestStack.White.UIItems.PropertyGridItems;

namespace AutomatedTests
{
    [TestFixture]
    public class UnitTest1
    {
        static Application application = Application.Launch("C:\\Program Files\\Notepad++\\notepad++.exe");
        static Window window = application.GetWindow("new 1 - Notepad++", InitializeOption.NoCache);
        MenuBar menuBar = window.MenuBar;


        [Test]
        public void CreatingNewFile()
        {


            Menu level2Menu = menuBar.MenuItem("File");
            //level2Menu = menuBar.MenuItemBy(SearchCriteria.ByText("File")); //can use any other search criteria as well.
            level2Menu.Click();
            Menu popupMenu = window.Get<Menu>("New");
            //Menu popupMenu = level2Menu.SubMenu("New");
            Thread.Sleep(2000);
            popupMenu.Click();
            Thread.Sleep(5 * 1000);
            //Assert.NotNull(application.GetWindow("new 2 - Notepad++"));
            Assert.AreEqual(window.Title,"new 2 - Notepad++", "Tytul sie nie zgadza");
        }

        [Test]
        public void OpeningExistingFile()
        {

            Menu level2Menu = menuBar.MenuItem("File");
            level2Menu.Click();
            Thread.Sleep(2000);

            Menu popupMenu = window.Get<Menu>("Open...");
            popupMenu.Click();
            Thread.Sleep(2000);

            string exFile = "E:\\UP KMIK\\II stopien\\2 semestr\\Testowanie oprogramowania - projekt\\193top1vhrabovyy\\AutomationTests\\AutomatedTests";
            string file = "test.txt";
           // exFile =exFile+ "\\" + file;

            //Window dialogOpen = application.GetWindow("Open", InitializeOption.NoCache);
            Window dialogOpen = null;
            List<Window> windows = application.GetWindows();
            System.Diagnostics.Debug.WriteLine("Initial List...");
            foreach (Window a in windows)
            {
                if (a.ToString().Contains("Open"))
                    dialogOpen = a;
            }
            ToolStrip tb = dialogOpen.Get<ToolStrip>(SearchCriteria.ByAutomationId("1001"));

            Button prevLoc = dialogOpen.Get<Button>(SearchCriteria.ByText("Previous Locations"));
            prevLoc.Click();
            Thread.Sleep(1000);

            TextBox ed = dialogOpen.Get<TextBox>(SearchCriteria.ByText("Address"));
            ed.SetValue(exFile);
            Thread.Sleep(1000);

            Button btGo;
            try
            {
                btGo = dialogOpen.Get<Button>(SearchCriteria.ByText("Go to \"" + exFile + "\""));
                if (btGo != null)
                    btGo.Click();

            }
            catch (AutomationException e)
            {
                btGo = dialogOpen.Get<Button>(SearchCriteria.ByText("Refresh \"" + exFile + "\" (F5)"));
                btGo.Click();
                btGo.Click();
            }
            Thread.Sleep(1000);
            btGo.Click();
            Thread.Sleep(1000);
            TextBox edFileName = dialogOpen.Get<TextBox>(SearchCriteria.ByText("File name:"));
            edFileName.SetValue(file);
            Thread.Sleep(1000);

            Button btOpen = dialogOpen.Get<Button>(SearchCriteria.ByText("Open"));
            btOpen.Click();
            Thread.Sleep(3*1000);
            Assert.AreEqual(window.Title, exFile + "\\" + file + " - Notepad++", "tytul sie nie zgadza");

        }


       

        [Test]
        public void ChangeEncodingOfCurrentFile()
        {
            OpeningExistingFile("encoding");
            Menu level2Menu = menuBar.MenuItem("Encoding");
            level2Menu.Click();
            Thread.Sleep(1000);

            Menu popupMenu = window.Get<Menu>("Encode in ANSI");
            popupMenu.Click();
            Thread.Sleep(5 * 1000);
            TextBox tb = window.Get<TextBox>(SearchCriteria.ByAutomationId("StatusBar.Pane4"));
            
            Assert.AreEqual(tb.Name, "ANSI");

            level2Menu.Click();
            Thread.Sleep(1000);

            popupMenu = window.Get<Menu>("Encode in UTF-8");
            popupMenu.Click();
            Assert.AreEqual(tb.Name, "UTF-8");
            Thread.Sleep(5 * 1000);


        }


        [Test]
        public void ConvertToUpperCase()
        {
            OpeningExistingFile("encoding");


            Menu level2Menu = menuBar.MenuItem("Edit");
            level2Menu.Click();
            Thread.Sleep(1000);

            Menu selectAll = window.Get<Menu>("Select All");
            selectAll.Click();
            Thread.Sleep(1000);

            level2Menu.Click();
            Menu popupMenu = window.Get<Menu>("Convert Case to");
            popupMenu.Click();
            Thread.Sleep(1000);

            Menu popupMenu1 = window.Get<Menu>("UPPERCASE");
            popupMenu1.Click();
            Thread.Sleep(5 * 1000);
        }

        [Test]
        public void ModifyText()
        {
            OpeningExistingFile("encoding");

            var textBox = window.Get(SearchCriteria.ByControlType(ControlType.Pane));

            if (textBox.ToString().Contains("1"))
            {
                textBox.Click();
                Menu level2Menu = menuBar.MenuItem("Edit");
                level2Menu.Click();
                Thread.Sleep(500);

                Menu selectAll = window.Get<Menu>("Select All");
                selectAll.Click();
                Thread.Sleep(500);
                for(int i=0; i<20; i++)
                    textBox.Enter((i+1)+": Żółwie bawią się na słońcu \n");
                Thread.Sleep(2 * 1000);

                Menu x = menuBar.MenuItem("X");
                x.Click();
                Thread.Sleep(500);

                Button popupMenu1 = window.Get<Button>("Yes");
                popupMenu1.Click();
                Thread.Sleep(500);


                Thread.Sleep(3 * 1000);

                OpeningExistingFile("encoding");
            }
      
        }


        [Test]
        public void AboutNotepad()
        {

            Menu level2Menu = menuBar.MenuItem("?");
            level2Menu.Click();
            Thread.Sleep(2000);

            Menu popupMenu = window.Get<Menu>("About Notepad++");
            popupMenu.Click();
            Thread.Sleep(10 * 1000);
            Button btOk = window.Get<Button>(SearchCriteria.ByText("OK"));
            Assert.NotNull(application.GetWindows().ToArray().Length>1);
            Assert.IsTrue(btOk.Visible, "Przycisk nie widoczny");

            btOk.Click();

        }




        public void OpeningExistingFile(string filename)
        {

            Menu level2Menu = menuBar.MenuItem("File");
            level2Menu.Click();
            Thread.Sleep(500);

            Menu popupMenu = window.Get<Menu>("Open...");
            popupMenu.Click();
            Thread.Sleep(1000);

            string exFile = "E:\\UP KMIK\\II stopien\\2 semestr\\Testowanie oprogramowania - projekt\\193top1vhrabovyy\\AutomationTests\\AutomatedTests";
            string file = filename;
            // exFile =exFile+ "\\" + file;

            //Window dialogOpen = application.GetWindow("Open", InitializeOption.NoCache);
            Window dialogOpen = null;
            List<Window> windows = application.GetWindows();
            System.Diagnostics.Debug.WriteLine("Initial List...");
            foreach (Window a in windows)
            {
                if (a.ToString().Contains("Open"))
                    dialogOpen = a;
            }
            ToolStrip tb = dialogOpen.Get<ToolStrip>(SearchCriteria.ByAutomationId("1001"));

            Button prevLoc = dialogOpen.Get<Button>(SearchCriteria.ByText("Previous Locations"));
            prevLoc.Click();
            Thread.Sleep(500);

            TextBox ed = dialogOpen.Get<TextBox>(SearchCriteria.ByText("Address"));
            ed.SetValue(exFile);
            Thread.Sleep(500);
            Button btGo;
            try { btGo = dialogOpen.Get<Button>(SearchCriteria.ByText("Go to \"" + exFile + "\""));
                if (btGo != null)
                    btGo.Click();
           
            }
            catch(AutomationException e)
            {
                btGo = dialogOpen.Get<Button>(SearchCriteria.ByText("Refresh \"" + exFile + "\" (F5)"));
                btGo.Click();
                btGo.Click();
            }

            Thread.Sleep(500);
        
            btGo.Click();
            Thread.Sleep(500);
            TextBox edFileName = dialogOpen.Get<TextBox>(SearchCriteria.ByText("File name:"));
            edFileName.SetValue(file);
            Thread.Sleep(500);

            Button btOpen = dialogOpen.Get<Button>(SearchCriteria.ByText("Open"));
            btOpen.Click();
            Thread.Sleep(3 * 1000);


        }

    }
}
