# Lista Scenariuszy Testowych
**Aplikacja**: Notepad++

Testy:
1. Utworzenie nowego pliku tekstowego - A
2. Otwieranie istniejącego pliku tekstowego - A
3. Zmiana kodowania dokumentu tekstowego - A
4. Zamiana wszystkich liter w tekscie na duże - A
5. Modyfikacja zawartośi pliku tekstowego - A
6. Wyszukiwanie wyrazu w tekscie
7. Zastąpienie wyrazu innym
8. Generowanie szyfru SHA-256 dla danego pliku
9. Przechodzenie do podanego wierszu w dokumencie
10. Sprawdzenie podświetlania składni dla języka C++
11. Funkcja zawijania tekstu
12. Wyświetlenie wszystkich znaków(w tym białych znaków)
13. Zmiana trybu wyświetlenia na pełnoekranowy
14. Wyświetlenie statystyk pliku
15. Wyświetlenie informacji o Notepad++ - A
